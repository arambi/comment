
<?= $this->loadHelper( 'Comment.Comment')->hiddens() ?>
<div class="c-comment-form__fields">
  <?php if( $this->Auth->user()): ?>
    <?= $this->Form->hidden( 'name', [
      'value' => $this->Auth->user( 'name')
    ]) ?>
  <?php else: ?>
    <div class="c-comment-form__fields__field c-comment-form__fields__field--text">

      <?php if($label_type=="placeholder") : ?>
        <?= $this->Form->input( 'name', [
          'placeholder' => __d( 'app', 'Nombre'),
          'label' => false
        ]) ?>
      <?php else : ?>
        <?= $this->Form->input( 'name', [
          'label' => __d( 'app', 'Nombre')
        ]) ?>
      <?php endif; ?>
      
      
    </div>
  <?php endif ?>


  
  <?php if( $this->Auth->user()): ?>
    <?= $this->Form->hidden( 'email', [
      'value' => $this->Auth->user( 'email')
    ]) ?>
  <?php else: ?>
    <div class="c-comment-form__fields__field c-comment-form__fields__field--text">

      <?php if($label_type=="placeholder") : ?>
        <?= $this->Form->input( 'email', [
          'placeholder' => __d( 'app', 'Email'),
          'label' => false
        ]) ?>
      <?php else : ?>
        <?= $this->Form->input( 'email', [
          'label' => __d( 'app', 'Email')
        ]) ?>
      <?php endif; ?>

    </div>
  <?php endif ?>
  
  <div class="c-comment-form__fields__field c-comment-form__fields__field--textarea">

      <?php if($label_type=="placeholder") : ?>
        <?= $this->Form->input( 'body', [
          'placeholder' => __d( 'app', 'Comentario'),
          'label' => false
        ]) ?>
      <?php else : ?>
        <?= $this->Form->input( 'body', [
          'label' => __d( 'app', 'Comentario'),
          'type' => 'textarea'
        ]) ?>
      <?php endif; ?>

  </div>
</div>

<div class="c-comment-form__bottom">
  <div class="c-comment-form__bottom__recaptcha">
    <?= $this->loadHelper( 'Recaptcha.Recaptcha')->display() ?>
    <?php if( isset( $recaptchaError)): ?>
      <div class="recaptcha-error"><?= $recaptchaError ?></div>
    <?php endif ?>
  </div>
  <div class="c-comment-form__bottom__submit">
    <?= $this->Form->button( __d( 'app', 'Enviar')) ?>
  </div>
</div>
<?= $this->loadHelper( 'Comment.Comment')->script() ?>