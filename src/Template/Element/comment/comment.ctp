<li>
  <div class="comment-info">
    <span class="comment-author"><?= $comment->name ?></span> <span class="comment-date"><?= $comment->date( 'created') ?></span>
  </div>
  <div class="comment-text">
    <p><?= $this->loadHelper( 'Cofree.Util')->linkify( $comment->body, ['http'], ['target' => '_blank']) ?></p>
  </div>
</li>
