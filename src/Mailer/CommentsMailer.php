<?php

namespace Comment\Mailer;

use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;
use Letter\Mailer\QueueMailer;
use EmailQueue\EmailQueue;
use Website\Lib\Website;
use Cake\Routing\Router;

/**
 * Comments mailer.
 */
class CommentsMailer extends QueueMailer
{
    use LetterTrait;

    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'Comments';


    public function newComment($comment)
    {
        $model = TableRegistry::getTableLocator()->get($comment->plugin_model);

        $content = $model->find()
            ->where([
                $model->getAlias() . '.' . $model->getPrimaryKey() => $comment->content_id
            ])
            ->first();

        if ($content) {
            $content_title = $content->get($model->getDisplayField());
        } else {
            $content_title = '';
        }


        $this->letter = LetterCollector::get('Comment.new_comment', [
            'web_name' => Website::get('title'),
            'content_title' => $content_title,
            'user_name' => $comment->name,
            'user_email' => $comment->email,
        ]);

        $this->_setLetterParams([]);

        $this->_email->setFrom(Website::get('settings.users_reply_email'));
        $this->_email->setTo(Website::get('settings.comments_email'));
    }
}
