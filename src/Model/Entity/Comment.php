<?php
namespace Comment\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Comment Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $model
 * @property string $foreign_key
 * @property string $body
 * @property string $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Comment extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];

  protected $_virtual = [
    'content'
  ];

  protected function _getContent()
  {
    $model = TableRegistry::get( $this->plugin_model);
    $content = $model->find()
      ->where([
        $model->getAlias() .'.'. $model->getPrimaryKey() => $this->content_id
      ])
      ->first();
    
    if( $content)
    {
      return '<a target="_blank" href="'. $content->link( 'slug') .'">'. $content->{$model->getDisplayField()}. '</a>';
    }
  }
}
