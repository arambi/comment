<?php

namespace Comment\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Comment\Model\Entity\Comment;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Website\Lib\Website;

/**
 * Comments Model
 */
class CommentsTable extends Table
{

    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';
    const STATUS_PENDENT = 'pendent';


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('comments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        $this->__setStatuses();

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud->order([
            'Comments.status',
            'Comments.created'
        ]);

        $this->crud
            ->addFields([
                'name' => __d('admin', 'Nombre'),
                'email' => __d('admin', 'Email'),
                'body' => __d('admin', 'Comentario'),
                'status' => [
                    'label' => __d('admin', 'Estado'),
                    'type' => 'select',
                    'options' => $this->_statuses
                ],
                'content' => [
                    'label' => 'Contenido',
                    'type' => 'info'
                ]

            ])
            ->addIndex('index', [
                'fields' => [
                    'name',
                    'email',
                    'content',
                    'body',
                    'status',
                    'created' => [
                        'label' => __d('app', 'Fecha'),
                        'type' => 'date'
                    ]
                ],
                'actionButtons' => [],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Comentarios'),
                'plural' => __d('admin', 'Comentarios'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'name',
                                    'email',
                                    'content',
                                    'body',
                                    'status',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['index']
            ], ['update'])
            ->order([
                'Comments.created' => 'desc'
            ]);
    }

    public function afterSave(Event $event, EntityInterface $entity)
    {
        if (!$entity->content_id) {
            $entity = $this->get($entity->id);
        }

        $count = $this->find()
            ->where([
                'Comments.status' => 'approved',
                'Comments.content_id' => $entity->content_id,
                'Comments.model' => $entity->model,
            ])
            ->count();

        $Table = TableRegistry::getTableLocator()->get($entity->plugin_model);

        if ($Table->hasField('comment_count')) {
            $content = $Table->find()
                ->where([
                    $Table->getAlias() . '.id' => $entity->content_id
                ])
                ->first();

            if ($content) {
                $content = $Table->get($entity->content_id);
                $content->set('comment_count', $count);
                $Table->save($content);
            }
        }
    }

    private function __setStatuses()
    {
        $this->_statuses = [
            'approved' => __d('app', 'Aprobado'),
            'rejected' => __d('app', 'Rechazado'),
            'pendent' => __d('app', 'Pendiente'),
        ];
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmptyString('name', __d('app', 'Es necesario indicar un nombre'))
            ->notEmptyString('body', __d('app', 'Es necesario escribir algo'))
            ->allowEmptyString('email')

            ->add('email', 'valid', [
                'rule' => 'email',
                'message' => __d('app', 'Tiene que ser un email válido')
            ])
            ->add('accept', 'equalTo', [
                'rule' => ['equalTo', '1'],
                'message' => __d('app', 'Es necesario aceptar la política de privacidad'),
            ]);

        if (Website::get('settings.blog_commentaries_email_mandatory')) {
            $validator
                ->notEmptyString('email', __d('app', 'Es necesario indicar un email'));
        }

        return $validator;
    }
}
