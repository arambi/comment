<?php
namespace Comment\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * Comment behavior
 */
class CommentBehavior extends Behavior
{

  public function initialize( array $config) 
  {
    $this->_table->hasMany( 'Comments', [
      'foreignKey' => 'content_id', 
      'className' => 'Comment.Comments',
      'sort' => ['Comments.created' => 'desc'],
      'conditions' => [
        'Comments.model' => $this->_table->alias()
      ]
    ]);


  }
}
