<?php
namespace Comment\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Comment helper
 */
class CommentHelper extends Helper
{
  public $helpers = ['Html', 'Form', 'Cofree.Buffer'];

/**
 * El id de la etiqueta <form> 
 * @var string
 */
  protected $_id;


/**
 * Toma el contenido principal de la vista
 * 
 * @return Entity | null
 */
  public function getContent()
  {
    $vars = $this->_View->getVars();
    
    if( in_array( 'content', $vars))
    {
      return $this->_View->get( 'content');
    }
  }

/**
 * Devuelve el formulario del comentario
 * Se renderiza el element comment/form que tiene que estar el el plugin o theme (de otro modo se renderiza el que hay por defecto en Comment)
 * 
 * @return string HTML
 */
  public function form($options=[])
  {
    $defaults = [
      'label_type'=> "label"
    ];
    $options = array_merge($defaults, $options);

    $this->_View->loadHelper( 'Recaptcha.Recaptcha');
    $vars = $this->_View->getVars();

    if( !in_array( 'newcomment', $vars))
    {
      $newcomment = TableRegistry::get( 'Comment.Comments')->newEntity();
      $this->_View->set( 'newcomment', $newcomment);
    }
    else
    {
      $newcomment = $this->_View->get( 'newcomment');
    }

    $out = [];

    $out [] = $this->Form->create( $newcomment, [
      'id' => $this->id(),
      'class' => 'comment-form',
      'url' => [
        'action' => 'comment',
      ]
    ]);

    $out [] = $this->_View->element( 'Comment.comment/form',$options);
    $out [] = $this->Form->end();
      
    return implode( "\n", $out);
  }

/**
 * Devuelve el id de la etiqueta <form> del formulario
 * 
 * @return string
 */
  public function id()
  {
    if( empty( $this->_id))
    {
      $this->_id = Text::uuid();
    }

    return $this->_id;
  }

/**
 * Devuelve o guarda un buffer del script del formulario (se envia por ajax)
 * 
 * @return string |null
 */
  public function script()
  {
    if( !$this->request->is( 'ajax'))
    {
      $this->Buffer->start();
    }
      
    $script = <<<EOF
      (function(){
        $("#{$this->id()}").submit(function(){
          var _this = this;
          $.ajax({
            type: 'post',
            url: $(this).attr( 'action'),
            data: $(this).serialize(),
            beforeSend: function(){
              $('button', _this).attr( 'disabled', 'disabled');
              $("input[type=submit]")
                .attr( 'disabled', true);
            },
            success: function( data){
              $("#{$this->id()}").html( data);
            },
            error: function(){
              $('button', this).attr( 'disabled', false);
              $("input[type=submit]")
                .attr( 'disabled', false);
            }
          })
          return false;
        })
      })();
EOF;
  
    if( !$this->request->is( 'ajax'))
    {
      echo $this->Html->scriptBlock( $script);    
      $this->Buffer->end();
    }
    else
    {
      return $this->Html->scriptBlock( $script);
    }
  }

/**
 * Devuelve los campos ocultos del formulario (content_id y model)
 * 
 * @return string HTML
 */
  public function hiddens()
  {
    $content = $this->getContent();

    $out = [];

    $out [] = $this->Form->hidden( 'content_id', [
      'value' => $this->request->data( 'content_id') ? $this->request->data( 'content_id') : $content->id
     ]);


    if( $content)
    {
      $model = $this->getModel();
    }
    else
    {
      $model = $this->request->data( 'model');
    }

    $out [] = $this->Form->hidden( 'model', [
      'value' => $model
    ]);

    return implode( "\n", $out);

  }

/**
 * Devuelve el nombre del model del contenido de la vista
 * 
 * @return string
 */
  public function getModel()
  {
    $content = $this->getContent();

    if( $content)
    {
      list( $plugin, $model) = pluginSplit( $content->source());
      return $model;
    }

    return null;
  }

/**
 * Devuelve los comentarios vinculados al contenido de la vista
 * Se renderiza de forma iterada el element comment/comment que tiene que estar el el plugin o theme (de otro modo se renderiza el que hay por defecto en Comment)
 * 
 * @return string HTML
 */
  public function comments( $limit = null)
  {
    $content = $this->getContent();

    if( $content == null)
    {
      return;
    }

    $Comments = TableRegistry::get( 'Comment.Comments');
    $comments = $Comments->find()
      ->where([
        'content_id' => $content->id,
        'model' => $this->getModel(),
        'status' => $Comments::STATUS_APPROVED
      ])
      ->order([
        'created' => 'desc'
      ]);

    $out = [];
    
    $close_limit = false;

    foreach( $comments as $key => $comment)
    {
      if( $limit && $key == $limit)
      {
        $out [] = '<div class="c-comments-more-link"><span>'. __d( 'app', 'Mostrar más comentarios') .'</span></div>';
        $out [] = '<div id="comments-more" style="display: none">';
        $close_limit = true;
      }

      $out [] = $this->_View->element( 'Comment.comment/comment', [
        'comment' => $comment
      ]);
    }

    if( $close_limit)
    {
      $out [] = '</div>';

      $this->Buffer->start();
      
      $script = <<<EOF
      (function(){
        $(".c-comments-more-link span").click(function(){
          $("#comments-more").show();
          $(this).remove();
          return false;
        })
      })();
EOF;
      echo $this->Html->scriptBlock( $script);    

      $this->Buffer->end();
    }

    return implode( "\n", $out);
  }
}