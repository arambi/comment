<?php
namespace Comment\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Comment\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \Comment\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController
{
    use CrudControllerTrait;
}
