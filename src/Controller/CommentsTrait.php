<?php

namespace Comment\Controller;

use Cake\Core\App;
use Website\Lib\Website;
use Letter\Mailer\QueueEmail;
use Cake\Mailer\Exception\MissingMailerException;



trait CommentsTrait
{

    public function getCustomMailer($name, QueueEmail $email = null)
    {
        if ($email === null) {
            $email = new QueueEmail();
        }

        $className = App::className($name, 'Mailer', 'Mailer');

        if (empty($className)) {
            throw new MissingMailerException(compact('name'));
        }

        return (new $className($email));
    }

    public function comment()
    {
        if (!$this->request->is('ajax')) {
            $this->Section->notFound();
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->loadComponent('Cofree.Recaptcha');
        $this->loadComponent('RequestHandler');

        $this->loadModel('Comment.Comments');
        $newcomment = $this->Comments->newEntity();
        $this->Comments->patchEntity($newcomment, $this->request->data);

        if ($this->request->is(['patch', 'post', 'put'])) {

            list($plugin, $model) = pluginSplit($this->modelClass);
            $newcomment->set('model', $model);
            $newcomment->set('plugin_model', $this->modelClass);
            $newcomment->set('status', 'pendent');

            if ($this->Recaptcha->verify() && $this->Comments->save($newcomment)) {
                if (Website::get('settings.comments_email')) {
                    $this->getCustomMailer('Comment.Comments')->send('newComment', [$newcomment]);
                }
                $this->render('comment_success');
            } else {
                $this->render('comment_form');
            }
        }

        $this->set(compact('newcomment'));
    }
}
