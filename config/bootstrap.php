<?php 
use Section\Action\ActionCollection;
 
use Manager\Navigation\NavigationCollection;
 
use User\Auth\Access;
 
require_once 'letters.php';



Access::add( 'comments', [
  'name' => 'Comentarios',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Comment',
          'controller' => 'Comments',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Comentarios',
  'parentName' => 'Comentarios',
  'plugin' => 'Comment',
  'controller' => 'Comments',
  'action' => 'index',
  'icon' => 'fa fa-comments-o',
]);
