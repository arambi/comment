<?php

use Phinx\Migration\AbstractMigration;

class PluginModelColumn extends AbstractMigration
{
   
  public function up()
  {
    $comments = $this->table( 'comments');
    $comments
      ->addColumn( 'plugin_model', 'string', ['null' => false, 'limit' => 32])
      ->update();
  }
  
  public function down()
  {
    $comments = $this->table( 'comments');
    $comments->removeColumn( 'plugin_model')->update();
  }
}
