<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
  public function up()
  {
    // Tabla para los models de contenido
    $sites = $this->table( 'comments');
    $sites
      ->addColumn( 'name', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'email', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'model', 'string', ['null' => false, 'limit' => 32])
      ->addColumn( 'parent_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 32])
      ->addColumn( 'content_id', 'string', ['null' => false, 'default' => NULL, 'limit' => 32])
      ->addColumn( 'body', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'status', 'string', ['null' => false, 'limit' => 255])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['status'])
      ->addIndex( ['created'])
      ->addIndex( ['content_id'])
      ->save();
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $this->dropTable( 'comments');
  }
}
