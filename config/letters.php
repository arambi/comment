<?php

use Letter\Collector\LetterCollector;


LetterCollector::add('Comment.new_comment', [
    'title' => 'Nuevo comentario',
    'subject' => [
        'spa' => 'Se ha hecho un nuevo comentario en {web_name}'
    ],
    'file' => 'new_comment',
    'vars' => [
        'web_name' => 'El nombre del web',
        'content_title' => 'El título del contenido',
        'user_name' => 'El nombre del usuario',
        'user_email' => 'El email de usuario',
    ]
]);
