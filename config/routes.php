<?php
use Cake\Routing\Router;

Router::plugin(
    'Comment',
    ['path' => '/comment'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
