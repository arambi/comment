<?php
namespace Comment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Comment\Model\Table\CommentsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Comment\Model\Table\CommentsTable Test Case
 */
class CommentsTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.comment.comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Comments') ? [] : ['className' => 'Comment\Model\Table\CommentsTable'];
        $this->Comments = TableRegistry::get('Comments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Comments);
        $this->assertCrudDataIndex( 'index', $this->Comments);
      }
}
