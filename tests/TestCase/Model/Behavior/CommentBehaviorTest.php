<?php
namespace Comment\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Comment\Model\Behavior\CommentBehavior;

/**
 * Comment\Model\Behavior\CommentBehavior Test Case
 */
class CommentBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Comment = new CommentBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comment);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
