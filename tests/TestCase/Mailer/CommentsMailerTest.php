<?php
namespace Comment\Test\TestCase\Mailer;

use Cake\TestSuite\TestCase;
use Comment\Mailer\CommentsMailer;

/**
 * Comment\Mailer\CommentsMailer Test Case
 */
class CommentsMailerTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Comment\Mailer\CommentsMailer
     */
    public $Comments;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
